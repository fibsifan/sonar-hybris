FROM sonarqube:latest
ARG revision=0.8.0-SNAPSHOT
ADD target/sonar-hybris-${revision}.jar /opt/sonarqube/extensions/plugins/
