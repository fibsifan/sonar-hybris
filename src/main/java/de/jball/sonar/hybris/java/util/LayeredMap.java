package de.jball.sonar.hybris.java.util;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This is a map which has a parent. The parent is searched for get(), size() and isEmpty().
 */
public class LayeredMap<K, V> implements Map<K, V> {
	Map<K, V> delegate;
	LayeredMap<K, V> parent;

	public LayeredMap() {
		delegate = new HashMap<>();
	}

	protected LayeredMap(LayeredMap<K, V> parent) {
		this();
		this.parent = parent;
	}

	@Nonnull
	public LayeredMap<K, V> createChild() {
		return new LayeredMap<>(this);
	}

	@Nullable
	public LayeredMap<K, V> getParent() {
		return parent;
	}

	@Override
	public int size() {
		return delegate.size() + (parent != null ? parent.size() : 0);
	}

	@Override
	public boolean isEmpty() {
		return delegate.isEmpty() && (parent == null || parent.isEmpty());
	}

	@Override
	public boolean containsKey(Object key) {
		return delegate.containsKey(key) || (parent != null && parent.containsKey(key));
	}

	@Override
	public boolean containsValue(Object value) {
		return delegate.containsValue(value) || (parent != null && parent.containsValue(value));
	}

	@Override
	public V get(Object key) {
		if (delegate.containsKey(key)) {
			return delegate.get(key);
		} else if (parent != null) {
			return parent.get(key);
		} else {
			return null;
		}
	}

	@Override
	public V put(K key, V value) {
		return delegate.put(key, value);
	}

	@Override
	public V remove(Object key) {
		return delegate.remove(key);
	}

	@Override
	public void putAll(@Nonnull Map<? extends K, ? extends V> m) {
		delegate.putAll(m);
	}

	/**
	 * Does not clear parent.
	 */
	@Override
	public void clear() {
		delegate.clear();
	}

	@Nonnull
	@Override
	public Set<K> keySet() {
		throw new UnsupportedOperationException("Not yet implemented.");
	}

	@Nonnull
	@Override
	public Collection<V> values() {
		throw new UnsupportedOperationException("Not yet implemented.");
	}

	/**
	 * @return only the entries of this Layer.
	 */
	@Nonnull
	@Override
	public Set<Entry<K, V>> entrySet() {
		return delegate.entrySet();
	}
}
