package de.jball.sonar.hybris.java;

import org.sonar.api.Plugin;

/**
 * Entry point of the plugin
 */
public class HybrisJavaRulesPlugin implements Plugin {
	@Override
	public void define(Context context) {
		// server extensions -> objects are instantiated during server startup
		context.addExtension(HybrisJavaRulesDefinition.class);

		// batch extensions -> objects are instantiated during code analysis
		context.addExtension(HybrisJavaFileCheckRegistrar.class);
	}
}
