package de.jball.sonar.hybris.java.checks;

import org.apache.commons.collections4.CollectionUtils;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.semantic.Symbol;
import org.sonar.plugins.java.api.tree.MethodInvocationTree;
import org.sonar.plugins.java.api.tree.Tree;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static de.jball.sonar.hybris.java.util.JavaHybrisUtils.isMethodFrom;

@Rule(key = "AvoidConfig")
public class AvoidConfig extends IssuableSubscriptionVisitor {
	private static final List<Tree.Kind> NODES_TO_VISIT = Collections.singletonList(Tree.Kind.METHOD_INVOCATION);
	private static final Set<String> METHOD_NAMES = Set.of(
			"getDouble",
			"getString",
			"getInt",
			"getChar",
			"getBoolean",
			"getLong"
	);
	private static final String CONFIG_FQDN = "de.hybris.platform.util.Config";

	@Override
	public List<Tree.Kind> nodesToVisit() {
		return NODES_TO_VISIT;
	}

	@Override
	public void visitNode(final Tree tree) {
		MethodInvocationTree methodInvocationTree = (MethodInvocationTree) tree;
		if (isConfigGetter(methodInvocationTree)) {
			reportIssue(tree, "Use the corresponding method of configurationService.getConfig() instead.");
		}
	}

	private boolean isConfigGetter(final MethodInvocationTree methodInvocationTree) {
		Symbol.MethodSymbol methodSymbol = methodInvocationTree.methodSymbol();

		return (isMethodFrom(methodSymbol, CONFIG_FQDN) // enclosingClass is null only for package symbols
				&& METHOD_NAMES.contains(methodSymbol.name()) //
				&& CollectionUtils.size(methodSymbol.parameterTypes()) == 2
				&& methodSymbol.parameterTypes().get(0).is("java.lang.String"));
	}
}
