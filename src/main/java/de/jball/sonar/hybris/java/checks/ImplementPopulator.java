package de.jball.sonar.hybris.java.checks;

import org.apache.commons.collections4.CollectionUtils;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.semantic.Symbol;
import org.sonar.plugins.java.api.semantic.Type;
import org.sonar.plugins.java.api.tree.ClassTree;
import org.sonar.plugins.java.api.tree.MethodTree;
import org.sonar.plugins.java.api.tree.Tree;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Rule(key = "ImplementPopulator")
public class ImplementPopulator extends IssuableSubscriptionVisitor {

	protected static final List<String> CREATE_TARGET_METHOD_NAMES = Arrays.asList("createTarget", "createFromClass");

	@Override
	public List<Tree.Kind> nodesToVisit() {
		return Collections.singletonList(Tree.Kind.CLASS);
	}

	@Override
	public void visitNode(Tree tree) {
		handleClass((ClassTree) tree);
	}

	private void handleClass(ClassTree classTree) {
		Type type = classTree.symbol().type();
		if (type.isSubtypeOf("de.hybris.platform.converters.impl.AbstractConverter")) {
			Collection<Symbol> classMembers = type.symbol().memberSymbols();
			reportIfTargetInstantiationMissing(classTree, classMembers);
		}
	}

	private void reportIfTargetInstantiationMissing(ClassTree classTree, Collection<Symbol> classMembers) {
		if (!hasConverterTargetInstantiation(classMembers)) {
			reportIssue(classTree, "Consider implementing Populator instead of extending AbstractConverter.");
		}
	}

	private boolean hasConverterTargetInstantiation(@Nonnull Collection<Symbol> classMembers) {
		List<MethodTree> createTargetMethods = classMembers.stream()
				.filter(this::isMethod)
				.map(Symbol::declaration)
				.map(MethodTree.class::cast)
				.filter(Objects::nonNull)
				.filter(this::isCreateTargetMethod)
				.collect(Collectors.toList());
		return CollectionUtils.isNotEmpty(createTargetMethods);
	}

	private boolean isMethod(Symbol member) {
		Tree declaration = member.declaration();
		return declaration != null && Tree.Kind.METHOD.equals(declaration.kind());
	}

	private boolean isCreateTargetMethod(@Nonnull MethodTree methodTree) {
		return CREATE_TARGET_METHOD_NAMES.contains(methodTree.simpleName().name()) &&
				CollectionUtils.isEmpty(methodTree.parameters());
	}
}
