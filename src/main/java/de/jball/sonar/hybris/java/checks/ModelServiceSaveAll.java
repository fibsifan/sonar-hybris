package de.jball.sonar.hybris.java.checks;

import org.apache.commons.collections4.CollectionUtils;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.semantic.Symbol;
import org.sonar.plugins.java.api.tree.MethodInvocationTree;
import org.sonar.plugins.java.api.tree.Tree;

import java.util.Collections;
import java.util.List;

import static de.jball.sonar.hybris.java.util.JavaHybrisUtils.isMethodFrom;

@Rule(key = "ModelServiceSaveAll")
public class ModelServiceSaveAll extends IssuableSubscriptionVisitor {
	private static final List<Tree.Kind> NODES_TO_VISIT = Collections.singletonList(Tree.Kind.METHOD_INVOCATION);
	private static final String MODEL_SERVICE_FQDN = "de.hybris.platform.servicelayer.model.ModelService";

	@Override
	public List<Tree.Kind> nodesToVisit() {
		return NODES_TO_VISIT;
	}

	@Override
	public void visitNode(final Tree tree) {
		MethodInvocationTree methodInvocationTree = (MethodInvocationTree) tree;
		if (isModelServiceSaveAll(methodInvocationTree)) {
			reportIssue(tree, "Use one of the other save methods of ModelService instead.");
		}
	}

	private boolean isModelServiceSaveAll(final MethodInvocationTree methodInvocationTree) {
		Symbol.MethodSymbol methodSymbol = methodInvocationTree.methodSymbol();

		return (isMethodFrom(methodSymbol, MODEL_SERVICE_FQDN) // enclosingClass is null only for package symbols
				&& "saveAll".equals(methodSymbol.name()) //
				&& CollectionUtils.isEmpty(methodInvocationTree.arguments()));
	}
}
