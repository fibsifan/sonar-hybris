package de.jball.sonar.hybris.java;

import de.jball.sonar.hybris.java.checks.*;
import de.jball.sonar.hybris.java.testchecks.TestAnnotation;
import org.sonar.plugins.java.api.JavaCheck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class RulesList {
	private RulesList() {
	}

	public static List<Class<? extends JavaCheck>> getChecks() {
		List<Class<? extends JavaCheck>> checks = new ArrayList<>();
		checks.addAll(getJavaChecks());
		checks.addAll(getJavaTestChecks());
		return Collections.unmodifiableList(checks);
	}

	public static List<Class<? extends JavaCheck>> getJavaChecks() {
		return List.of(ModelConstructor.class,
				ModelServiceSaveAll.class,
				AvoidJalo.class,
				ImplementPopulator.class,
				AvoidManualTransactionHandling.class);
	}

	public static List<Class<? extends JavaCheck>> getJavaTestChecks() {
		return Collections.singletonList(TestAnnotation.class);
	}
}
