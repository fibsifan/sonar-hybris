package de.jball.sonar.hybris.java.util;

import org.sonar.plugins.java.api.semantic.Symbol;

public final class JavaHybrisUtils {
	private JavaHybrisUtils() {
		// don't instantiate
	}

	public static boolean isMethodFrom(Symbol.MethodSymbol methodSymbol, String className) {
		Symbol.TypeSymbol typeSymbol = methodSymbol.enclosingClass();
		return typeSymbol.type().isSubtypeOf(className);
	}
}
