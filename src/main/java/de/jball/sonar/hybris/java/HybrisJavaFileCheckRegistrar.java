package de.jball.sonar.hybris.java;

import org.sonar.plugins.java.api.CheckRegistrar;
import org.sonar.plugins.java.api.JavaCheck;
import org.sonarsource.api.sonarlint.SonarLintSide;

import java.util.List;

/**
 * Provide the "checks" (implementations of rules) classes that are going be executed during
 * source code analysis.
 * <p>
 * This class is a batch extension by implementing the {@link org.sonar.plugins.java.api.CheckRegistrar} interface.
 */
@SonarLintSide
public class HybrisJavaFileCheckRegistrar implements CheckRegistrar {

	public static List<Class<? extends JavaCheck>> checkClasses() {
		return RulesList.getJavaChecks();
	}

	public static List<Class<? extends JavaCheck>> testCheckClasses() {
		return RulesList.getJavaTestChecks();
	}

	@Override
	public void register(final RegistrarContext registrarContext) {
		// Call to registerClassesForRepository to associate the classes with the correct repository key
		registrarContext.registerClassesForRepository(HybrisJavaRulesDefinition.REPOSITORY_KEY, checkClasses(), testCheckClasses());
	}
}
