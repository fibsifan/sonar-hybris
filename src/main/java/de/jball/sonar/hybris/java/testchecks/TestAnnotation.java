package de.jball.sonar.hybris.java.testchecks;


import org.apache.commons.collections4.CollectionUtils;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.JavaFileScannerContext;
import org.sonar.plugins.java.api.semantic.Symbol;
import org.sonar.plugins.java.api.semantic.Type;
import org.sonar.plugins.java.api.tree.AnnotationTree;
import org.sonar.plugins.java.api.tree.ClassTree;
import org.sonar.plugins.java.api.tree.ExpressionTree;
import org.sonar.plugins.java.api.tree.MethodTree;
import org.sonar.plugins.java.api.tree.Tree;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Rule(key = "TestAnnotation")
public class TestAnnotation extends IssuableSubscriptionVisitor {
	@Override
	public List<Tree.Kind> nodesToVisit() {
		return Collections.singletonList(Tree.Kind.CLASS);
	}

	@Override
	public void visitNode(final Tree tree) {
		ClassTree classTree = (ClassTree) tree;

		if (classTree.symbol().isUnknown()) {
			return;
		}

		if (hasHybrisTestAnnotation(classTree)) {
			return;
		}

		final List<JavaFileScannerContext.Location> testMethodLocations = classTree.members().stream()
				.filter(this::isMethod)
				.map(MethodTree.class::cast)
				.filter(this::hasJunitTestAnnotation)
				.map(this::locationOfTestMethod)
				.collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(testMethodLocations)) {
			reportIssue(classTree,
					"Add the @UnitTest or @IntegrationTest to this class for it's tests to be executed.",
					testMethodLocations, null);
		}
	}

	private boolean hasHybrisTestAnnotation(final ClassTree classTree) {
		List<AnnotationTree> annotations = getAnnotationsIncludingParents(classTree);
		return annotations.stream()
				.map(ExpressionTree::symbolType)
				.anyMatch(this::isHybrisTestAnnotationType);
	}

	private List<AnnotationTree> getAnnotationsIncludingParents(final ClassTree classTree) {
		Spliterator<ClassTree> classHierarchySpliterator = new ClassHierarchySpliterator(classTree);
		return StreamSupport.stream(classHierarchySpliterator, true)
				.flatMap(c -> c.modifiers().annotations().stream())
				.collect(Collectors.toList());
	}

	private boolean isMethod(Tree tree) {
		return Tree.Kind.METHOD.equals(tree.kind());
	}

	private boolean isHybrisTestAnnotationType(Type type) {
		return type.is("de.hybris.bootstrap.annotations.UnitTest")
				|| type.is("de.hybris.bootstrap.annotations.IntegrationTest");
	}

	private boolean hasJunitTestAnnotation(MethodTree methodTree) {
		return methodTree.modifiers().annotations().stream()
				.anyMatch(this::isJunitTestAnnotation);
	}

	private boolean isJunitTestAnnotation(AnnotationTree annotation) {
		return annotation.annotationType().symbolType().is("org.junit.Test");
	}

	private JavaFileScannerContext.Location locationOfTestMethod(MethodTree methodTree) {
		return new JavaFileScannerContext.Location(
				"Add the @UnitTest or @IntegrationTest annotation to the class for this test to be executed.",
				methodTree);
	}

	private static class ClassHierarchySpliterator implements Spliterator<ClassTree> {
		private ClassTree currentClass;

		public ClassHierarchySpliterator(ClassTree classTree) {
			currentClass = classTree;
		}

		@Override
		public boolean tryAdvance(Consumer<? super ClassTree> consumer) {
			if (currentClass == null || currentClass.symbol().isUnknown() || currentClass.symbol().type().is("java.lang.Object")) {
				return false;
			} else {
				consumer.accept(currentClass);
				currentClass = Optional.of(currentClass)
						.map(ClassTree::symbol)
						.map(Symbol.TypeSymbol::superClass)
						.map(Type::symbol)
						.map(Symbol.TypeSymbol::declaration)
						.orElse(null);
				return true;
			}
		}

		@Override
		public Spliterator<ClassTree> trySplit() {
			return null;
		}

		@Override
		public long estimateSize() {
			return Long.MAX_VALUE;
		}

		@Override
		public int characteristics() {
			return Spliterator.ORDERED | Spliterator.DISTINCT | Spliterator.NONNULL | Spliterator.IMMUTABLE;
		}
	}
}
