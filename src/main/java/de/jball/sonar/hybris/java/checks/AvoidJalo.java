package de.jball.sonar.hybris.java.checks;

import org.apache.commons.collections4.CollectionUtils;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.semantic.Symbol;
import org.sonar.plugins.java.api.semantic.Type;
import org.sonar.plugins.java.api.tree.ExpressionTree;
import org.sonar.plugins.java.api.tree.MethodInvocationTree;
import org.sonar.plugins.java.api.tree.MethodTree;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.plugins.java.api.tree.VariableTree;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;

@Rule(key = "AvoidJalo")
public class AvoidJalo extends IssuableSubscriptionVisitor {
	private static final Set<Tree.Kind> NODES_TO_VISIT = EnumSet.of(
			Tree.Kind.METHOD,
			Tree.Kind.METHOD_INVOCATION,
			Tree.Kind.VARIABLE);

	@Override
	public List<Tree.Kind> nodesToVisit() {
		return List.copyOf(NODES_TO_VISIT);
	}

	@Override
	public void visitNode(final Tree tree) {
		switch (tree.kind()) {
			case METHOD:
				visitMethod((MethodTree) tree);
				break;
			case VARIABLE:
				visitVariable((VariableTree) tree);
				break;
			case METHOD_INVOCATION:
				visitMethodInvocation((MethodInvocationTree) tree);
				break;
			default:
		}
	}

	private void visitVariable(final VariableTree variable) {
		if (isJaloType(variable.type().symbolType())) {
			reportJalo(variable);
		}
	}

	private void visitMethod(final MethodTree method) {
		// Arguments are handled by #visitVariable

		if (methodHasJaloReturnType(method.symbol())) {
			reportJalo(method);
		}
	}

	private void visitMethodInvocation(MethodInvocationTree methodInvocation) {
		if (methodInvocation.methodSymbol().isUnknown()) {
			return;
		}

		if (methodHasJaloReturnType(methodInvocation.methodSymbol()) //
				|| methodHasJaloParameter(methodInvocation.methodSymbol())) {
			reportJalo(methodInvocation.methodSelect());
		} else if (methodInvocationWithJaloArg(methodInvocation)) {
			reportJalo(methodInvocation);
		}
	}

	private boolean methodHasJaloReturnType(final Symbol.MethodSymbol methodSymbol) {
		return isJaloType(methodSymbol.returnType().type());
	}

	private boolean methodHasJaloParameter(final Symbol.MethodSymbol methodSymbol) {
		return methodSymbol.parameterTypes().stream() //
				.anyMatch(this::isJaloType);
	}

	private boolean methodInvocationWithJaloArg(MethodInvocationTree methodInvocation) {
		return CollectionUtils.emptyIfNull(methodInvocation.arguments()).stream()
				.map(ExpressionTree::symbolType)
				.anyMatch(this::isJaloType);
	}

	private boolean isJaloType(final Type type) {
		return type.isSubtypeOf("de.hybris.platform.jalo.Item");
	}

	private void reportJalo(final Tree tree) {
		reportIssue(tree, "Avoid using jalo classes");
	}
}
