package de.jball.sonar.hybris.java.checks;

import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.semantic.Symbol;
import org.sonar.plugins.java.api.tree.MethodInvocationTree;
import org.sonar.plugins.java.api.tree.Tree;

import java.util.List;

import static de.jball.sonar.hybris.java.util.JavaHybrisUtils.isMethodFrom;

@Rule(key = "AvoidManualTransactionHandling")
public class AvoidManualTransactionHandling extends IssuableSubscriptionVisitor {
	@Override
	public List<Tree.Kind> nodesToVisit() {
		return List.of(Tree.Kind.METHOD_INVOCATION);
	}


	@Override
	public void visitNode(Tree tree) {
		handleMethodInvocation((MethodInvocationTree) tree);

	}

	/* default */ void handleMethodInvocation(MethodInvocationTree methodInvocationTree) {
		Symbol.MethodSymbol methodSymbol = methodInvocationTree.methodSymbol();
		if (isMethodFrom(methodSymbol, "de.hybris.platform.tx.Transaction") &&
				List.of("begin", "commit", "rollback").contains(methodSymbol.name())) {
			reportIssue(methodInvocationTree, "Avoid manual transaction handling and use Transaction.execute() instead.");
		}
	}
}
