package de.jball.sonar.hybris.java.checks;

import de.jball.sonar.hybris.java.util.LayeredMap;
import org.apache.commons.collections4.CollectionUtils;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.IssuableSubscriptionVisitor;
import org.sonar.plugins.java.api.semantic.Symbol;
import org.sonar.plugins.java.api.semantic.Type;
import org.sonar.plugins.java.api.tree.AssignmentExpressionTree;
import org.sonar.plugins.java.api.tree.ExpressionTree;
import org.sonar.plugins.java.api.tree.IdentifierTree;
import org.sonar.plugins.java.api.tree.MemberSelectExpressionTree;
import org.sonar.plugins.java.api.tree.MethodInvocationTree;
import org.sonar.plugins.java.api.tree.NewClassTree;
import org.sonar.plugins.java.api.tree.Tree;
import org.sonar.plugins.java.api.tree.VariableTree;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Rule(key = "ModelConstructor")
public class ModelConstructor extends IssuableSubscriptionVisitor {
	private static final List<Tree.Kind> NODES_TO_VISIT = Arrays.asList(//
			Tree.Kind.NEW_CLASS,//
			Tree.Kind.BLOCK,//
			Tree.Kind.COMPILATION_UNIT,//
			Tree.Kind.VARIABLE);
	private static final String MODEL_SERVICE_FQDN = "de.hybris.platform.servicelayer.model.ModelService";
	private static final String FLEXIBLE_SEARCH_SERVICE_FQDN = "de.hybris.platform.servicelayer.search.FlexibleSearchService";
	public static final String USE_MODEL_SERVICE_CREATE_INSTEAD = "Use \"modelService.create()\" instead.";
	/*default*/ LayeredMap<VariableTree, List<ExpressionTree>> assignedExpressionsByVariable;

	public ModelConstructor() {
		assignedExpressionsByVariable = new LayeredMap<>();
	}

	@Override
	public List<Tree.Kind> nodesToVisit() {
		return NODES_TO_VISIT;
	}

	@Override
	public void visitNode(final Tree tree) {
		if (tree.is(Tree.Kind.NEW_CLASS)) {
			NewClassTree newClassTree = (NewClassTree) tree;
			Type typeOfNewObject = newClassTree.symbolType();
			if (typeOfNewObject.is("de.hybris.platform.core.model.ItemModel") || //
					typeOfNewObject.isSubtypeOf("de.hybris.platform.core.model.ItemModel")) {
				handleModelConstructor(newClassTree);
			}
		} else if (tree.is(Tree.Kind.BLOCK)) {
			createNewLayer();
		} else if (tree.is(Tree.Kind.VARIABLE)) {
			addVariableToCurrentLayer((VariableTree) tree);
		}
		// else nothing: TREE.KIND.COMPILATION_UNIT is handled when leaving the block, see #leaveNode

	}

	private void createNewLayer() {
		assignedExpressionsByVariable = assignedExpressionsByVariable.createChild();
	}

	private void leaveLayer() {
		assignedExpressionsByVariable = assignedExpressionsByVariable.getParent();
	}

	private void addVariableToCurrentLayer(VariableTree tree) {
		assignedExpressionsByVariable.putIfAbsent(tree, new CopyOnWriteArrayList<>());
	}

	@Override
	public void leaveNode(Tree tree) {
		if (tree.is(Tree.Kind.BLOCK)) {
			checkVariableUsage();
			leaveLayer();
		} else if (tree.is(Tree.Kind.COMPILATION_UNIT)) {
			assignedExpressionsByVariable.clear();
		}
	}

	private void handleModelConstructor(NewClassTree newClassTree) {
		if (newClassTree.parent().is(Tree.Kind.VARIABLE)) {
			final VariableTree variable = (VariableTree) newClassTree.parent();
			final List<ExpressionTree> assignmentsForVariable = assignedExpressionsByVariable.get(variable);
			assignmentsForVariable.add(variable.initializer());
		} else if (newClassTree.parent().is(Tree.Kind.ARGUMENTS)) {
			handleModelConstructorAsMethodArg(newClassTree);
		} else { // newClassTree.parent().is(Tree.Kind.ASSIGNMENT)
			final AssignmentExpressionTree assignmentExpression = (AssignmentExpressionTree) newClassTree.parent();
			VariableTree referencedVariable = getReferencedVariable(assignmentExpression);
			final List<ExpressionTree> assignmentsForVariable = assignedExpressionsByVariable.get(referencedVariable);
			assignmentsForVariable.add(assignmentExpression.expression());
		}
	}

	private void handleModelConstructorAsMethodArg(NewClassTree newClassTree) {
		if(!isModelServiceSearch((MethodInvocationTree) newClassTree.parent().parent()) &&
				!isFlexibleSearchServiceSearch((MethodInvocationTree) newClassTree.parent().parent())) {
			reportIssue(newClassTree, USE_MODEL_SERVICE_CREATE_INSTEAD);
		}
	}

	private void checkVariableUsage() {
		assignedExpressionsByVariable.forEach((variable, assignedExpressions) -> {
			if (variable.symbol().usages().stream().anyMatch(this::isSaved) || //
					variable.symbol().usages().stream().noneMatch(this::isUsedAsSearchExample)) {
				assignedExpressions.forEach(expression ->
						reportIssue(expression, USE_MODEL_SERVICE_CREATE_INSTEAD));
			}
		});
	}

	private VariableTree getReferencedVariable(AssignmentExpressionTree assignmentExpression) {
		if(assignmentExpression.variable().is(Tree.Kind.IDENTIFIER)) {
			return getVariableFromIdentifier((IdentifierTree) assignmentExpression.variable());
		} else {
			throw new UnsupportedOperationException("Getting variable of " + assignmentExpression.variable().kind() +
					" is not supported");
		}
	}

	private VariableTree getVariableFromIdentifier(IdentifierTree identifier) {
		return (VariableTree) identifier.symbol().declaration();
	}

	private boolean isUsedAsSearchExample(IdentifierTree identifierTree) {
		if (usedInMethodInvocation(identifierTree)) {
			final MethodInvocationTree invocation = (MethodInvocationTree) identifierTree.parent().parent();
			return isModelServiceSearch(invocation)
					|| isFlexibleSearchServiceSearch(invocation);
		}
		return false;
	}

	private boolean isModelServiceSearch(MethodInvocationTree invocation) {
		if (invocation.methodSelect().is(Tree.Kind.MEMBER_SELECT) &&
				isModelService(invocation) &&
				"getByExample".equals(((MemberSelectExpressionTree)invocation.methodSelect()).identifier().name())) {
			final Symbol.MethodSymbol symbol = (Symbol.MethodSymbol) ((MemberSelectExpressionTree) invocation.methodSelect()).identifier().symbol();
			final List<Type> types = symbol.parameterTypes();
			return CollectionUtils.size(types) == 1 && types.get(0).is(symbol.returnType().type().fullyQualifiedName());
		}
		return false;
	}

	private boolean isFlexibleSearchServiceSearch(MethodInvocationTree invocation) {
		if (invocation.methodSelect().is(Tree.Kind.MEMBER_SELECT) &&
				isFlexibleSearchService(invocation)) {
			final String methodName = ((MemberSelectExpressionTree) invocation.methodSelect()).identifier().name();
			if ("getModelByExample".equals(methodName)) {
				final Symbol.MethodSymbol symbol = (Symbol.MethodSymbol) ((MemberSelectExpressionTree) invocation.methodSelect()).identifier().symbol();
				final List<Type> types = symbol.parameterTypes();
				return CollectionUtils.size(types) == 1 && types.get(0).is(symbol.returnType().type().fullyQualifiedName());
			} else if ("getModelsByExample".equals(methodName)) {
				final Symbol.MethodSymbol symbol = (Symbol.MethodSymbol) ((MemberSelectExpressionTree) invocation.methodSelect()).identifier().symbol();
				final List<Type> types = symbol.parameterTypes();
				return CollectionUtils.size(types) == 1 && "java.util.List".equals(symbol.returnType().type().fullyQualifiedName());
			}
		}
		return false;
	}

	private boolean isModelService(MethodInvocationTree invocation) {
		return isMethodInvocationFor(invocation, MODEL_SERVICE_FQDN);
	}

	private boolean isFlexibleSearchService(MethodInvocationTree invocation) {
		return isMethodInvocationFor(invocation, FLEXIBLE_SEARCH_SERVICE_FQDN);
	}

	private boolean isMethodInvocationFor(MethodInvocationTree invocation, String targetClassName) {
		if (invocation.methodSelect().is(Tree.Kind.MEMBER_SELECT)) {
			return ((MemberSelectExpressionTree) invocation.methodSelect()).expression().symbolType().isSubtypeOf(targetClassName);
		}
		return false;
	}

	private boolean isSaved(IdentifierTree identifierTree) {
		if(usedInMethodInvocation(identifierTree)) {
			return isModelService(((MethodInvocationTree)identifierTree.parent().parent()))
					&& isModelServiceSave(((MethodInvocationTree)identifierTree.parent().parent()));
		}
		return false;
	}

	private boolean isModelServiceSave(MethodInvocationTree methodInvocation) {
		if (methodInvocation.methodSelect().is(Tree.Kind.MEMBER_SELECT)
				&& "save".equals(((MemberSelectExpressionTree)methodInvocation.methodSelect()).identifier().name())) {
			final Symbol.MethodSymbol symbol = (Symbol.MethodSymbol) ((MemberSelectExpressionTree) methodInvocation.methodSelect()).identifier().symbol();
			final List<Type> types = symbol.parameterTypes();
			return CollectionUtils.size(types) == 1;
		}
		return false;
	}

	private boolean usedInMethodInvocation(IdentifierTree identifierTree) {
		return identifierTree.parent().is(Tree.Kind.ARGUMENTS) && identifierTree.parent().parent().is(Tree.Kind.METHOD_INVOCATION);
	}
}
