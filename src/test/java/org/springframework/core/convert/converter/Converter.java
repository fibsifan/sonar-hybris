package org.springframework.core.convert.converter;

/**
 * @deprecated Only for use in test-examples. Do not use in actual test code or in rule-code.
 */
@Deprecated
public interface Converter<S, T> {
	T convert(S s);
}
