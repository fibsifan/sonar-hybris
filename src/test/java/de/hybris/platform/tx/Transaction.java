package de.hybris.platform.tx;

/**
 * @deprecated Only for use in test-examples. Do not use in actual test code or in rule-code.
 */
@Deprecated
public class Transaction {
	private static final Transaction INSTANCE = new Transaction();


	public void begin() {}
	public void commit() {}

	public Object execute(TransactionBody transactionBody) throws Exception {
		return null;
	}

	public void rollback() {}

	public static Transaction current() {
		return INSTANCE;
	}
}
