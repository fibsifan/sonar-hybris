package de.hybris.platform.tx;

/**
 * @deprecated Only for use in test-examples. Do not use in actual test code or in rule-code.
 */
@Deprecated
public interface TransactionBody {
	<T> T execute();
}
