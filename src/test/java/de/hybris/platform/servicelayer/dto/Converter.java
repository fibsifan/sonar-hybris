package de.hybris.platform.servicelayer.dto;

import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @deprecated Only for use in test-examples. Do not use in actual test code or in rule-code.
 */
@Deprecated
public interface Converter<SOURCE, TARGET> extends org.springframework.core.convert.converter.Converter<SOURCE, TARGET> {
	TARGET convert(SOURCE source, TARGET prototype);

	default List<TARGET> convertAll(Collection<? extends SOURCE> sources) {
		return CollectionUtils.emptyIfNull(sources).stream().map(this::convert).collect(Collectors.toList());
	}

	default List<TARGET> convertAllIgnoreExceptions(Collection<? extends SOURCE> sources) {
		// that's feasible for this dummy
		return convertAll(sources);
	}
}
