package de.hybris.platform.servicelayer.model;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

/**
 * @deprecated Only for use in test-examples. Do not use in actual test code or in rule-code.
 */
@Deprecated
public class ModelService {
	public <T> T create(Class<T> clazz) {
		try {
			return clazz.getDeclaredConstructor(new Class[]{}).newInstance();
		} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
			return null;
		}
	}

	public <T> T getByExample(T example) {
		return null;
	}

	public void save(Object o) {
	}

	public void saveAll() {
	}

	public void saveAll(Collection<?> coll) {
	}

	public void saveAll(Object... objects) {
	}

	public <T> T getSource(Object o) {
		return null;
	}

	public <T extends Collection> T getAllSources(Collection<?> c, T ct) {
		return ct;
	}

	public <T> T get(Object o) {
		return null;
	}

	Class getModelTypeClass(Class c) {
		return c;
	}
}
