package de.hybris.platform.servicelayer.search;

import java.util.Collections;
import java.util.List;

/**
 * @deprecated Only for use in test-examples. Do not use in actual test code or in rule-code.
 */
@Deprecated
public class FlexibleSearchService {
	public <T> T getModelByExample(T example) {
		return null;
	}

	public <T> List<T> getModelsByExample(T example) {
		return Collections.emptyList();
	}
}
