package de.hybris.platform.core.model;

/**
 * @deprecated Only for use in test-examples. Do not use in actual test code or in rule-code.
 */
@Deprecated
public class ItemModel {
}
