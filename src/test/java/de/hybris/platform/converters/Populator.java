package de.hybris.platform.converters;

/**
 * @deprecated Only for use in test-examples. Do not use in actual test code or in rule-code.
 */
@Deprecated
public interface Populator<SOURCE, TARGET> {
	void populate(SOURCE source, TARGET target);
}
