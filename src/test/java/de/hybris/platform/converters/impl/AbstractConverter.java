package de.hybris.platform.converters.impl;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.Converter;

/**
 * @deprecated Only for use in test-examples. Do not use in actual test code or in rule-code.
 */
@Deprecated
public abstract class AbstractConverter<SOURCE, TARGET> implements Converter<SOURCE, TARGET>, Populator<SOURCE, TARGET> {
	private Class<? extends TARGET> targetClass;

	@Override
	public TARGET convert(SOURCE source, TARGET prototype) {
		populate(source, prototype);
		return prototype;
	}

	@Override
	public TARGET convert(SOURCE source) {
		TARGET target = createTarget();
		populate(source, target);
		return target;
	}

	protected TARGET createFromClass() {
		try {
			return targetClass.newInstance();
		} catch (IllegalAccessException | InstantiationException e) {
			throw new RuntimeException(e);
		}
	}

	@Deprecated
	protected TARGET createTarget() {
		return createFromClass();
	}

	public void setTargetClass(Class<? extends TARGET> targetClass) {
		this.targetClass = targetClass;
	}
}
