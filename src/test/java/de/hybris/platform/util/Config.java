package de.hybris.platform.util;

import org.apache.commons.lang3.StringUtils;

/**
 * @deprecated Only for use in test-examples. Do not use in actual test code or in rule-code.
 */
@Deprecated
public abstract class Config {
	public static boolean getBoolean(String key, boolean def) {
		return def;
	}

	public static char getChar(String key, char def) {
		return def;
	}

	public static double getDouble(String key, double def) {
		return def;
	}

	public static int getInt(String key, int def) {
		return def;
	}

	public static long getLong(String key, long def) {
		return def;
	}

	public static String getString(String key, String def) {
		return def;
	}

	public static String getDatabase() {
		return StringUtils.EMPTY;
	}
}
