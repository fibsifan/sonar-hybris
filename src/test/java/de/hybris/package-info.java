/**
 * Classes for use in Test examples.
 *
 * @deprecated Do not use in production code, do not use in rule-code.
 */
package de.hybris;
