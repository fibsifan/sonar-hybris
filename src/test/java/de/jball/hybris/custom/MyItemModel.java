package de.jball.hybris.custom;

import de.hybris.platform.core.model.ItemModel;

/**
 * Extension of  ItemModel for Tests.
 *
 * @deprecated do not use in production code, do not use in rule code.
 */
@Deprecated
public class MyItemModel extends ItemModel {
}
