package de.jball.hybris.custom;

import de.hybris.platform.jalo.Item;

/**
 * Extension of jalo Item for Tests.
 *
 * @deprecated do not use in production code, do not use in rule code.
 */
@Deprecated
public class MyItem extends Item {
}
