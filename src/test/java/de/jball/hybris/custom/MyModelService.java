package de.jball.hybris.custom;

import de.hybris.platform.servicelayer.model.ModelService;

/**
 * Extension of ModelService for Tests.
 *
 * @deprecated do not use in production code, do not use in rule code.
 */
@Deprecated
public class MyModelService extends ModelService {
}
