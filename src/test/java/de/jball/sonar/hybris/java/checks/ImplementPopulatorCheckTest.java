package de.jball.sonar.hybris.java.checks;

import de.jball.sonar.hybris.java.AbstractRuleCheckTest;

import javax.annotation.Nonnull;

public class ImplementPopulatorCheckTest extends AbstractRuleCheckTest<ImplementPopulator> {
	@Nonnull
	@Override
	protected String getCheckFilename() {
		return "src/test/resources/ImplementPopulatorCheck.java";
	}

	@Nonnull
	@Override
	protected ImplementPopulator getRuleInstance() {
		return new ImplementPopulator();
	}
}
