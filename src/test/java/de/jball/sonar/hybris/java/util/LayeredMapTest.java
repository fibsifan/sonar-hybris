package de.jball.sonar.hybris.java.util;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

class LayeredMapTest {
	@Mock
	private LayeredMap<String, String> parent;
	@Mock
	private Map<String, String> delegate;
	@Spy
	private LayeredMap<String, String> unitUnderTest;
	private AutoCloseable openMocks;

	static Stream<Arguments> intArgs() {
		return intArg().flatMap(intArg1 -> intArg().map(intArg2 -> Arguments.of(intArg1, intArg2)));
	}

	private static Stream<Integer> intArg() {
		return Stream.of(0, 1, 2);
	}

	static Stream<Arguments> booleanArgs() {
		return booleanArg().flatMap(booleanArg1 -> booleanArg().map(booleanArg2 -> Arguments.of(booleanArg1, booleanArg2)));
	}

	private static Stream<Boolean> booleanArg() {
		return Stream.of(true, false);
	}

	@BeforeEach
	public void setUp() {
		openMocks = MockitoAnnotations.openMocks(this);
		unitUnderTest.delegate = delegate;
		unitUnderTest.parent = parent;
		doReturn(true).when(parent).containsKey("keyInParent");
		doReturn("valueInParent").when(parent).get("keyInParent");
		doReturn(true).when(parent).containsValue("valueInParent");
		doReturn(true).when(delegate).containsKey("keyInDelegate");
		doReturn("valueInDelegate").when(delegate).get("keyInDelegate");
		doReturn(true).when(delegate).containsValue("valueInDelegate");
	}

	@AfterEach
	public void tearDown() throws Exception {
		openMocks.close();
	}

	@ParameterizedTest
	@MethodSource("intArgs")
	void createChild(int parentSize, int delegateSize) {
		// given
		LayeredMap<String, String> child = unitUnderTest.createChild();
		doReturn(parentSize).when(parent).size();
		doReturn(delegateSize).when(delegate).size();
		// when
		int result = child.size();
		// then
		verify(unitUnderTest).size();
		verify(delegate).size();
		assertEquals(parentSize + delegateSize, result);
	}

	@Test
	void getParent() {
		// when
		LayeredMap<String, String> result = unitUnderTest.getParent();
		// then
		assertEquals(parent, result);
	}

	@Test
	void getNullParent() {
		// given
		unitUnderTest.parent = null;
		// when
		final LayeredMap<String, String> result = unitUnderTest.getParent();
		// then
		assertNull(result);
	}

	@ParameterizedTest
	@MethodSource("intArgs")
	void sizeWithParent(int parentSize, int delegateSize) {
		// given
		doReturn(parentSize).when(parent).size();
		doReturn(delegateSize).when(delegate).size();
		// when
		int result = unitUnderTest.size();
		// then
		verify(parent).size();
		verify(delegate).size();
		assertEquals(parentSize + delegateSize, result);
	}

	@ParameterizedTest
	@MethodSource("intArg")
	void sizeWithoutParent(int delegateSize) {
		// given
		doReturn(delegateSize).when(delegate).size();
		unitUnderTest.parent = null;
		// when
		int result = unitUnderTest.size();
		// then
		verify(delegate).size();
		verify(parent, never()).size();
		assertEquals(delegateSize, result);
	}

	@ParameterizedTest
	@MethodSource("booleanArgs")
	void isEmptyWithParent(boolean parentEmpty, boolean delegateEmpty) {
		// given
		doReturn(parentEmpty).when(parent).isEmpty();
		doReturn(delegateEmpty).when(delegate).isEmpty();
		// when
		boolean result = unitUnderTest.isEmpty();
		// then
		verify(delegate).isEmpty();
		if (delegateEmpty) {
			verify(parent).isEmpty();
		}
		assertEquals(parentEmpty && delegateEmpty, result);
	}

	@ParameterizedTest
	@MethodSource("booleanArg")
	void isEmptyWithoutParent(boolean delegateEmpty) {
		// given
		doReturn(delegateEmpty).when(delegate).isEmpty();
		unitUnderTest.parent = null;
		// when
		boolean result = unitUnderTest.isEmpty();
		// then
		verify(delegate).isEmpty();
		verify(parent, never()).isEmpty();
		assertEquals(delegateEmpty, result);
	}

	@Test
	void parentContainsKey() {
		// when
		boolean result = unitUnderTest.containsKey("keyInParent");
		// then
		verify(delegate).containsKey("keyInParent");
		verify(parent).containsKey("keyInParent");
		assertTrue(result);
	}

	@Test
	void delegateContainsKey() {
		// when
		boolean result = unitUnderTest.containsKey("keyInDelegate");
		// then
		verify(delegate).containsKey("keyInDelegate");
		verify(parent, never()).containsValue(anyString());
		assertTrue(result);
	}

	@Test
	void parentContainsValue() {
		// when
		boolean result = unitUnderTest.containsValue("valueInParent");
		// then
		verify(delegate).containsValue("valueInParent");
		verify(parent).containsValue("valueInParent");
		assertTrue(result);
	}

	@Test
	void delegateContainsValue() {
		// given
		doReturn(true).when(delegate).containsValue("valueInDelegate");
		// when
		boolean result = unitUnderTest.containsValue("valueInDelegate");
		// then
		verify(delegate).containsValue("valueInDelegate");
		verify(parent, never()).containsValue(anyString());
		assertTrue(result);
	}

	@Test
	void getFromParent() {
		// when
		String value = unitUnderTest.get("keyInParent");
		// then
		verify(parent).get("keyInParent");
		verify(delegate).containsKey("keyInParent");
		verify(delegate, never()).get("keyInParent");
		assertEquals("valueInParent", value);
	}

	@Test
	void getFromDelegate() {
		// when
		String value = unitUnderTest.get("keyInDelegate");
		// then
		verify(delegate).containsKey("keyInDelegate");
		verify(delegate).get("keyInDelegate");
		verify(parent, never()).containsKey("keyInDelegate");
		verify(parent, never()).get("keyInDelegate");
		assertEquals("valueInDelegate", value);
	}

	@Test
	void getWithoutParent() {
		// given
		unitUnderTest.parent = null;
		// when
		String result = unitUnderTest.get("keyInParent");
		// then
		verify(delegate).containsKey("keyInParent");
		verify(parent, never()).containsKey(anyString());
		assertNull(result);
	}

	@Test
	void dontPutInParent() {
		// given
		LayeredMap<String, String> child = unitUnderTest.createChild();
		// when
		child.put("Test2", "Test2");
		// then
		verify(unitUnderTest, never()).put(anyString(), anyString());
		assertTrue(child.containsKey("Test2"));
		assertEquals("Test2", child.get("Test2"));
	}

	@Test
	void putInDelegate() {
		// given
		doReturn("Test3").when(delegate).put("Test2", "Test2");
		// when
		String result = unitUnderTest.put("Test2", "Test2");
		// then
		verify(parent, never()).put(anyString(), anyString());
		verify(delegate).put("Test2", "Test2");
		assertEquals("Test3", result);
	}

	@Test
	void remove() {
		// when
		unitUnderTest.remove("Test");
		// then
		verify(parent, never()).remove(anyString());
		verify(delegate).remove("Test");
	}

	@Test
	void putAll() {
		// given
		Map<String, String> addAllMap = Mockito.mock(Map.class);
		// when
		unitUnderTest.putAll(addAllMap);
		// then
		verify(parent, never()).putAll(anyMap());
		verify(delegate).putAll(addAllMap);
	}

	@Test
	void clear() {
		// when
		unitUnderTest.clear();
		verify(parent, never()).clear();
		verify(delegate).clear();
	}
}
