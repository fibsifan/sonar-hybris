package de.jball.sonar.hybris.java;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.sonar.api.Plugin;

import static org.mockito.Mockito.verify;

class HybrisJavaRulesPluginTest {
	@Mock
	private Plugin.Context pluginContext;
	private AutoCloseable openMocks;

	@BeforeEach
	public void setUp() {
		openMocks = MockitoAnnotations.openMocks(this);
	}

	@AfterEach
	public void tearDown() throws Exception {
		openMocks.close();
	}

	@Test
	void testDefine() {
		// given
		HybrisJavaRulesPlugin hybrisJavaRulesPlugin = new HybrisJavaRulesPlugin();

		// when
		hybrisJavaRulesPlugin.define(pluginContext);
		// then
		verify(pluginContext).addExtension(HybrisJavaRulesDefinition.class);
		verify(pluginContext).addExtension(HybrisJavaFileCheckRegistrar.class);
	}
}
