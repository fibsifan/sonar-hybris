package de.jball.sonar.hybris.java;

import de.jball.sonar.hybris.java.checks.*;
import de.jball.sonar.hybris.java.testchecks.TestAnnotation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.sonar.plugins.java.api.CheckRegistrar;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.verify;

class HybrisJavaFileCheckRegistrarTest {
	@Mock
	private CheckRegistrar.RegistrarContext registrarContext;
	private AutoCloseable openMocks;

	@BeforeEach
	public void setUp() {
		openMocks = MockitoAnnotations.openMocks(this);
	}

	@AfterEach
	public void tearDown() throws Exception {
		openMocks.close();
	}

	@Test
	void testCheckClasses() {
		assertThat(HybrisJavaFileCheckRegistrar.checkClasses(), hasItem(ModelConstructor.class));
		assertThat(HybrisJavaFileCheckRegistrar.checkClasses(), hasItem(AvoidJalo.class));
		assertThat(HybrisJavaFileCheckRegistrar.checkClasses(), hasItem(ModelServiceSaveAll.class));
		assertThat(HybrisJavaFileCheckRegistrar.checkClasses(), hasItem(ImplementPopulator.class));
		assertThat(HybrisJavaFileCheckRegistrar.checkClasses(), hasItem(AvoidManualTransactionHandling.class));
	}

	@Test
	void testTestCheckClasses() {
		assertThat(HybrisJavaFileCheckRegistrar.testCheckClasses(), hasItem(TestAnnotation.class));
	}

	@Test
	void testRegister() {
		// given
		HybrisJavaFileCheckRegistrar registrar = new HybrisJavaFileCheckRegistrar();
		// when
		registrar.register(registrarContext);
		// then
		verify(registrarContext).registerClassesForRepository(HybrisJavaRulesDefinition.REPOSITORY_KEY,
				HybrisJavaFileCheckRegistrar.checkClasses(), HybrisJavaFileCheckRegistrar.testCheckClasses());
	}
}
