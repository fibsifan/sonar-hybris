package de.jball.sonar.hybris.java.checks;

import de.jball.sonar.hybris.java.AbstractRuleCheckTest;

import javax.annotation.Nonnull;

public class ModelServiceSaveAllCheckTest extends AbstractRuleCheckTest<ModelServiceSaveAll> {
	@Nonnull
	@Override
	protected String getCheckFilename() {
		return "src/test/resources/ModelServiceSaveAllCheck.java";
	}

	@Nonnull
	@Override
	protected ModelServiceSaveAll getRuleInstance() {
		return new ModelServiceSaveAll();
	}
}
