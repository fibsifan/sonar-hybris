package de.jball.sonar.hybris.java;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.sonar.api.rule.RuleScope;
import org.sonar.api.rule.RuleStatus;
import org.sonar.api.rules.RuleType;
import org.sonar.api.server.debt.DebtRemediationFunction;
import org.sonar.api.server.rule.RulesDefinition;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.JavaCheck;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class HybrisJavaRulesDefinitionTest {

	@Mock
	private RulesDefinition.Context mockRulesDefinitionContext;

	@Mock
	private RulesDefinition.NewRepository mockRepository;

	@Mock
	private RulesDefinition.NewRule mockModelConstructor;

	@Mock
	private RulesDefinition.NewRule mockAvoidJalo;

	@Mock
	private RulesDefinition.NewRule mockModelServiceSaveAll;

	@Mock
	private RulesDefinition.NewRule mockTestAnnotation;

	@Mock
	private RulesDefinition.NewRule mockImplementPopulator;

	@Mock
	private RulesDefinition.NewRule mockAvoidManualTransactionHandling;

	@Mock
	private RulesDefinition.DebtRemediationFunctions mockDebtRemediationFunctions;

	@Mock
	private DebtRemediationFunction mockDebtRemediationFunction;
	private AutoCloseable openMocks;

	@BeforeEach
	public void setUp() {
		openMocks = MockitoAnnotations.openMocks(this);

		doReturn(mockRepository).when(mockRulesDefinitionContext).createRepository("sonar-hybris-java", "java");
		doReturn(mockRepository).when(mockRepository).setName(anyString());

		wireRuleMock("ModelConstructor", mockModelConstructor);
		wireRuleMock("AvoidJalo", mockAvoidJalo);
		wireRuleMock("ModelServiceSaveAll", mockModelServiceSaveAll);
		wireRuleMock("TestAnnotation", mockTestAnnotation);
		wireRuleMock("ImplementPopulator", mockImplementPopulator);
		wireRuleMock("AvoidManualTransactionHandling", mockAvoidManualTransactionHandling);
	}

	@AfterEach
	public void tearDown() throws Exception {
		openMocks.close();
	}

	private void wireRuleMock(String ruleKey, RulesDefinition.NewRule ruleMock) {
		doReturn(ruleMock).when(mockRepository).createRule(ruleKey);
		doReturn(ruleMock).when(mockRepository).rule(ruleKey);
		doReturn(ruleMock).when(ruleMock).setName(any());
		doReturn(ruleKey).when(ruleMock).key();
		doReturn(mockDebtRemediationFunctions).when(ruleMock).debtRemediationFunctions();
	}

	@Test
	void testRepositoryDefinition() {
		// given
		final HybrisJavaRulesDefinition hybrisJavaRulesDefinition = new HybrisJavaRulesDefinition();

		// when
		hybrisJavaRulesDefinition.define(mockRulesDefinitionContext);

		// then
		verify(mockRulesDefinitionContext).createRepository("sonar-hybris-java", "java");
		verify(mockRepository).setName("Hybris Java Rules Repository");
		verify(mockRepository, times(6)).createRule(anyString());
	}

	@Test
	void testModelConstructorProperties() {
		// given
		final HybrisJavaRulesDefinition hybrisJavaRulesDefinition = new HybrisJavaRulesDefinition();
		doReturn(mockDebtRemediationFunction).when(mockDebtRemediationFunctions).constantPerIssue("5min");

		// when
		hybrisJavaRulesDefinition.define(mockRulesDefinitionContext);

		// then
		verify(mockRepository).createRule("ModelConstructor");
		verify(mockModelConstructor).setName("Don't call constructor for ItemModel or its subclasses directly");
		verify(mockModelConstructor).setType(RuleType.BUG);
		verify(mockModelConstructor, times(2)).setSeverity("MAJOR");
		verify(mockModelConstructor, times(2)).setStatus(RuleStatus.READY);
		verify(mockModelConstructor).setDebtRemediationFunction(mockDebtRemediationFunction);
		verify(mockModelConstructor).addTags("hybris", "pitfall");
		verify(mockModelConstructor).setScope(RuleScope.ALL);
		verify(mockDebtRemediationFunctions, atLeast(1)).constantPerIssue("5min");
	}

	@Test
	void testModelServiceSaveAllProperties() {
		// given
		final HybrisJavaRulesDefinition hybrisJavaRulesDefinition = new HybrisJavaRulesDefinition();
		doReturn(mockDebtRemediationFunction).when(mockDebtRemediationFunctions).constantPerIssue("5min");

		// when
		hybrisJavaRulesDefinition.define(mockRulesDefinitionContext);

		// then
		verify(mockRepository).createRule("ModelServiceSaveAll");
		verify(mockModelServiceSaveAll).setName("Avoid calling ModelService.saveAll()");
		verify(mockModelServiceSaveAll).setType(RuleType.BUG);
		verify(mockModelServiceSaveAll, times(2)).setSeverity("MAJOR");
		verify(mockModelServiceSaveAll, times(2)).setStatus(RuleStatus.READY);
		verify(mockModelServiceSaveAll).setDebtRemediationFunction(mockDebtRemediationFunction);
		verify(mockModelServiceSaveAll).addTags("hybris", "pitfall");
		verify(mockModelServiceSaveAll).setScope(RuleScope.ALL);
		verify(mockDebtRemediationFunctions, atLeast(1)).constantPerIssue("5min");
	}

	@Test
	void testAvoidJaloProperties() {
		// given
		final HybrisJavaRulesDefinition hybrisJavaRulesDefinition = new HybrisJavaRulesDefinition();
		doReturn(mockDebtRemediationFunction).when(mockDebtRemediationFunctions).constantPerIssue("5min");

		// when
		hybrisJavaRulesDefinition.define(mockRulesDefinitionContext);

		// then
		verify(mockRepository).createRule("AvoidJalo");
		verify(mockAvoidJalo).setName("Avoid using Jalo classes");
		verify(mockAvoidJalo).setType(RuleType.CODE_SMELL);
		verify(mockAvoidJalo).setSeverity("MINOR");
		verify(mockAvoidJalo, times(2)).setStatus(RuleStatus.READY);
		verify(mockAvoidJalo).setDebtRemediationFunction(mockDebtRemediationFunction);
		verify(mockAvoidJalo).addTags("hybris", "bad-practice");
		verify(mockAvoidJalo).setScope(RuleScope.ALL);
		verify(mockDebtRemediationFunctions, atLeast(1)).constantPerIssue("5min");
	}

	@Test
	void testImplementPopulatorProperties() {
		// given
		final HybrisJavaRulesDefinition hybrisJavaRulesDefinition = new HybrisJavaRulesDefinition();
		doReturn(mockDebtRemediationFunction).when(mockDebtRemediationFunctions).constantPerIssue("10min");

		// when
		hybrisJavaRulesDefinition.define(mockRulesDefinitionContext);

		// then
		verify(mockRepository).createRule("ImplementPopulator");
		verify(mockImplementPopulator).setName("Implement Populator");
		verify(mockImplementPopulator).setType(RuleType.CODE_SMELL);
		verify(mockImplementPopulator).setSeverity("MINOR");
		verify(mockImplementPopulator, times(2)).setStatus(RuleStatus.READY);
		verify(mockImplementPopulator).setDebtRemediationFunction(mockDebtRemediationFunction);
		verify(mockImplementPopulator).addTags("hybris", "bad-practice");
		verify(mockImplementPopulator).setScope(RuleScope.ALL);
		verify(mockDebtRemediationFunctions, atLeast(1)).constantPerIssue("10min");
	}

	@Test
	void testAvoidManualTransactionHandlingProperties() {
		// given
		final HybrisJavaRulesDefinition hybrisJavaRulesDefinition = new HybrisJavaRulesDefinition();
		doReturn(mockDebtRemediationFunction).when(mockDebtRemediationFunctions).constantPerIssue("10min");

		// when
		hybrisJavaRulesDefinition.define(mockRulesDefinitionContext);

		// then
		verify(mockRepository).createRule("AvoidManualTransactionHandling");
		verify(mockAvoidManualTransactionHandling).setName("Avoid manual transaction handling");
		verify(mockAvoidManualTransactionHandling).setType(RuleType.BUG);
		verify(mockAvoidManualTransactionHandling).setSeverity("CRITICAL");
		verify(mockAvoidManualTransactionHandling, times(2)).setStatus(RuleStatus.READY);
		verify(mockAvoidManualTransactionHandling).setDebtRemediationFunction(mockDebtRemediationFunction);
		verify(mockAvoidManualTransactionHandling).addTags("hybris", "bad-practice", "pitfall");
		verify(mockAvoidManualTransactionHandling).setScope(RuleScope.ALL);
		verify(mockDebtRemediationFunctions, atLeast(1)).constantPerIssue("10min");
	}

	@Test
	void testTestAnnotationProperties() {
		// given
		final HybrisJavaRulesDefinition hybrisJavaRulesDefinition = new HybrisJavaRulesDefinition();
		doReturn(mockDebtRemediationFunction).when(mockDebtRemediationFunctions).constantPerIssue("5min");

		// when
		hybrisJavaRulesDefinition.define(mockRulesDefinitionContext);

		// then
		verify(mockRepository).createRule("TestAnnotation");
		verify(mockTestAnnotation).setName("Annotate testclasses with @UnitTest or @IntegrationTest");
		verify(mockTestAnnotation).setType(RuleType.BUG);
		verify(mockTestAnnotation, times(2)).setSeverity("MAJOR");
		verify(mockTestAnnotation, times(2)).setStatus(RuleStatus.READY);
		verify(mockTestAnnotation).setDebtRemediationFunction(mockDebtRemediationFunction);
		verify(mockTestAnnotation).addTags("hybris", "pitfall");
		verify(mockTestAnnotation).setScope(RuleScope.TEST);
		verify(mockDebtRemediationFunctions, atLeast(1)).constantPerIssue("5min");
	}

	@Test
	void testNewRuleWithoutAnnotation() {
		// given
		HybrisJavaRulesDefinition hybrisJavaRulesDefinition = new HybrisJavaRulesDefinition();
		RulesDefinition.NewRepository newRepository = mock(RulesDefinition.NewRepository.class);

		// when
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> hybrisJavaRulesDefinition.newRule(RuleWithoutAnnotation.class, newRepository));

		// then
		Assertions.assertThat(exception.getMessage()).contains("No Rule annotation");
	}

	@Test
	void testNewRuleWithoutKey() {
		// given
		HybrisJavaRulesDefinition hybrisJavaRulesDefinition = new HybrisJavaRulesDefinition();
		RulesDefinition.NewRepository newRepository = mock(RulesDefinition.NewRepository.class);

		// when
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> hybrisJavaRulesDefinition.newRule(RuleWithoutKey.class, newRepository));

		// then
		Assertions.assertThat(exception.getMessage()).contains("No key is defined in Rule");
	}

	@Test
	void testUncreatedRule() {
		// given
		HybrisJavaRulesDefinition hybrisJavaRulesDefinition = new HybrisJavaRulesDefinition();
		RulesDefinition.NewRepository newRepository = mock(RulesDefinition.NewRepository.class);
		doReturn(null).when(newRepository).rule("testRuleKey");

		// when
		IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> hybrisJavaRulesDefinition.newRule(UncreatedRule.class, newRepository));

		// then
		Assertions.assertThat(exception.getMessage()).contains("No rule was created");
	}

	static class RuleWithoutAnnotation implements JavaCheck {
		// nothing here
	}

	@Rule
	static class RuleWithoutKey implements JavaCheck {
		// nothing here
	}

	@Rule(key = "testRuleKey")
	static class UncreatedRule implements JavaCheck {
		// nothing here
	}
}
