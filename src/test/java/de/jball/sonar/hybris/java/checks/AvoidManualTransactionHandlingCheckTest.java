package de.jball.sonar.hybris.java.checks;

import de.jball.sonar.hybris.java.AbstractRuleCheckTest;

import javax.annotation.Nonnull;

public class AvoidManualTransactionHandlingCheckTest extends AbstractRuleCheckTest<AvoidManualTransactionHandling> {
	@Nonnull
	@Override
	protected String getCheckFilename() {
		return "src/test/resources/AvoidManualTransactionHandlingCheck.java";
	}

	@Nonnull
	@Override
	protected AvoidManualTransactionHandling getRuleInstance() {
		return new AvoidManualTransactionHandling();
	}
}
