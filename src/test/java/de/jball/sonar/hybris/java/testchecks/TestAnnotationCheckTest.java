package de.jball.sonar.hybris.java.testchecks;

import de.jball.sonar.hybris.java.AbstractRuleCheckTest;

import javax.annotation.Nonnull;

public class TestAnnotationCheckTest extends AbstractRuleCheckTest<TestAnnotation> {
	@Nonnull
	@Override
	protected String getCheckFilename() {
		return "src/test/resources/TestAnnotationCheck.java";
	}

	@Nonnull
	@Override
	protected TestAnnotation getRuleInstance() {
		return new TestAnnotation();
	}
}
