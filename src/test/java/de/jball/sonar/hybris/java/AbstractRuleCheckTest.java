package de.jball.sonar.hybris.java;

import org.apache.commons.collections4.ListUtils;
import org.junit.jupiter.api.Test;
import org.sonar.java.checks.verifier.CheckVerifier;
import org.sonar.java.checks.verifier.FilesUtils;
import org.sonar.plugins.java.api.JavaFileScanner;

import javax.annotation.Nonnull;
import java.io.File;
import java.nio.file.Path;
import java.util.List;

public abstract class AbstractRuleCheckTest<T extends JavaFileScanner> {
	@Nonnull
	protected abstract String getCheckFilename();

	@Nonnull
	protected abstract T getRuleInstance();

	@Test
	public void check() {
		final List<File> testJars = FilesUtils.getFilesRecursively(Path.of("target/test-jars"), "jar");

		CheckVerifier.newVerifier()
				.onFile(getCheckFilename())
				.withCheck(getRuleInstance())
				.withClassPath(ListUtils.union(testJars, List.of(new File("target/test-classes"))))
				.verifyIssues();
	}
}
