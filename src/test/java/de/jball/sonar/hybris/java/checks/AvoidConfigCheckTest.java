package de.jball.sonar.hybris.java.checks;

import de.jball.sonar.hybris.java.AbstractRuleCheckTest;

import javax.annotation.Nonnull;

public class AvoidConfigCheckTest extends AbstractRuleCheckTest<AvoidConfig> {
	@Nonnull
	@Override
	protected String getCheckFilename() {
		return "src/test/resources/AvoidConfigCheck.java";
	}

	@Nonnull
	@Override
	protected AvoidConfig getRuleInstance() {
		return new AvoidConfig();
	}
}
