package de.jball.sonar.hybris.java.checks;

import de.jball.sonar.hybris.java.AbstractRuleCheckTest;

import javax.annotation.Nonnull;

public class AvoidJaloCheckTest extends AbstractRuleCheckTest<AvoidJalo> {
	@Nonnull
	@Override
	protected String getCheckFilename() {
		return "src/test/resources/AvoidJaloCheck.java";
	}

	@Nonnull
	@Override
	protected AvoidJalo getRuleInstance() {
		return new AvoidJalo();
	}
}
