package de.jball.sonar.hybris.java.checks;

import de.jball.sonar.hybris.java.AbstractRuleCheckTest;
import org.junit.jupiter.api.Test;

import javax.annotation.Nonnull;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ModelConstructorCheckTest extends AbstractRuleCheckTest<ModelConstructor> {
	private final ModelConstructor ruleInstance;

	public ModelConstructorCheckTest() {
		ruleInstance = new ModelConstructor();
	}

	@Test
	public void check() {
		super.check();
		assertTrue(getRuleInstance().assignedExpressionsByVariable.isEmpty());
	}

	@Nonnull
	@Override
	protected String getCheckFilename() {
		return "src/test/resources/ModelConstructorCheck.java";
	}

	@Nonnull
	@Override
	protected ModelConstructor getRuleInstance() {
		return this.ruleInstance;
	}
}
