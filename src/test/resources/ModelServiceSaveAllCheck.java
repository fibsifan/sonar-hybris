import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.jball.hybris.custom.MyModelService;

import java.util.Collection;

/**
 * This file is the sample code against we run our unit test.
 * It is placed src/test/resources in order to not be part of the compilation.
 **/
class ModelServiceSaveAllCheck {
	private ModelService modelService;
	private Collection<ItemModel> items;
	private ItemModel item;
	private MyModelService myModelService;

	public void modelServiceSaveAll() {
		modelService.saveAll(); // Noncompliant {{Use one of the other save methods of ModelService instead.}}
		modelService.save(item);
		modelService.saveAll(items);
		modelService.saveAll(item, item);
		myModelService.saveAll(); // Noncompliant {{Use one of the other save methods of ModelService instead.}}
		items.size();
	}
}
