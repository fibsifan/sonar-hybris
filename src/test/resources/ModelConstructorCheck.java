import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.jball.hybris.custom.MyItemModel;

import java.util.List;

/**
 * This file is the sample code against we run our unit test.
 * It is placed src/test/resources in order to not be part of the compilation.
 **/
class ModelConstructorCheck {
	private ModelService modelService;
	private FlexibleSearchService flexibleSearchService;

	public void saveItemModels() {
		ItemModel item = new ItemModel(); // Noncompliant {{Use "modelService.create()" instead.}}
		modelService.save(item);
		ItemModel item2 = modelService.create(ItemModel.class);
		MyItemModel myItem = new MyItemModel(); // Noncompliant {{Use "modelService.create()" instead.}}
		modelService.save(myItem);
		MyItemModel myItem2 = modelService.create(MyItemModel.class);
		Object test = new Object();
		ItemModel[] arrayTest = new ItemModel[0];
	}

	public void searchItemModels() {
		ItemModel item = new ItemModel();
		ItemModel item2 = new ItemModel(); // Noncompliant {{Use "modelService.create()" instead.}}
		ItemModel item3 = new ItemModel();
		ItemModel item4 = new ItemModel();
		ItemModel item5;

		ItemModel foundItem = modelService.getByExample(item);

		ItemModel foundItem2 = modelService.getByExample(item);
		if (foundItem2 == null) {
			modelService.save(item2);
		}

		item5 = new ItemModel(); // Noncompliant {{Use "modelService.create()" instead.}}

		ItemModel foundItem3 = flexibleSearchService.getModelByExample(item3);

		List<ItemModel> foundItems4 = flexibleSearchService.getModelsByExample(item4);

		modelService.save(new MyItemModel()); // Noncompliant {{Use "modelService.create()" instead.}}
		modelService.getByExample(new MyItemModel());
		flexibleSearchService.getModelsByExample(new MyItemModel());
	}
}
