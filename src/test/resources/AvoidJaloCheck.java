import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.model.ModelService;
import de.jball.hybris.custom.MyItem;
import de.jball.hybris.custom.MyItemModel;
import de.jball.hybris.undefined.JaloItem;

/**
 * This file is the sample code against we run our unit test.
 * It is placed src/test/resources in order to not be part of the compilation.
 **/
class AvoidJaloCheck {
	private ModelService modelService;
	private Item item; // Noncompliant {{Avoid using jalo classes}}
	private MyItem myItem; // Noncompliant {{Avoid using jalo classes}}
	private ItemModel itemModel;
	private MyItemModel myItemModel;
	private JaloItem jaloItem = new JaloItem();

	public void createItemModels() {
		itemModel =
				modelService.get(item); // Noncompliant {{Avoid using jalo classes}}
		itemModel =
				modelService.get( // Noncompliant {{Avoid using jalo classes}}
						getItem()); // Noncompliant {{Avoid using jalo classes}}
		item = modelService.getSource(itemModel); // Noncompliant {{Avoid using jalo classes}}
		item =
				getItem(); // Noncompliant {{Avoid using jalo classes}}

		myItem =
				getMyItem(); // Noncompliant {{Avoid using jalo classes}}
		myItem =
				modelService.getSource(myItemModel); // Noncompliant {{Avoid using jalo classes}}
		myItemModel =
				modelService.get(myItem); // Noncompliant {{Avoid using jalo classes}}

		jaloItem = jaloItem.method();
	}

	public Item getItem() { // Noncompliant {{Avoid using jalo classes}}
		return null;
	}

	public MyItem getMyItem() { // Noncompliant {{Avoid using jalo classes}}
		return null;
	}

	public void useItem(Item itemInMethod) { // Noncompliant {{Avoid using jalo classes}}
		// does nothing
	}

	public void useMyItem(MyItem itemInMethod) { // Noncompliant {{Avoid using jalo classes}}
		// does nothing
	}

	public <T extends Item> T convert(String chars, Class<T> clazz) { // Noncompliant {{Avoid using jalo classes}}
		return null;
	}
}
