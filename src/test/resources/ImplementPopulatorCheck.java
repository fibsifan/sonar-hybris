import de.hybris.platform.converters.impl.AbstractConverter;
import de.jball.hybris.custom.MyItem;
import de.jball.hybris.custom.MyItemModel;

/**
 * This file is the sample code against we run our unit test.
 * It is placed src/test/resources in order to not be part of the compilation.
 **/
class ImplementPopulatorCheck {
	public static class SimpleConverterImpl extends AbstractConverter<MyItemModel, MyItem> { // Noncompliant {{Consider implementing Populator instead of extending AbstractConverter.}}
		@Override
		public void populate(MyItemModel myItemModel, MyItem myItem) {
			// irrelevant
		}
	}

	public static class CreateFromClassConverterImpl extends AbstractConverter<MyItem, MyItemModel> {
		@Override
		public void populate(MyItem myItem, MyItemModel myItemModel) {
			// irrelevant
		}

		@Override
		protected MyItemModel createFromClass() {
			return null;
		}
	}

	public static class CreateTargetConverterImpl extends AbstractConverter<MyItem, MyItemModel> {
		@Override
		public void populate(MyItem myItem, MyItemModel myItemModel) {
			// irrelevant
		}

		@Override
		protected MyItemModel createTarget() {
			return null;
		}
	}
}
