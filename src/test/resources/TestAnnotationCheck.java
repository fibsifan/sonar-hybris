import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Assert;
import org.junit.Test;

/**
 * This file is the sample code against we run our unit test.
 * It is placed src/test/resources in order to not be part of the compilation.
 **/
class TestAnnotationCheck {
	public static class TestWithoutAnnotation { // Noncompliant {{Add the @UnitTest or @IntegrationTest to this class for it's tests to be executed.}}
		@Test
		public void test() {
			Assert.assertTrue(true);
		}
	}

	public static class NoTestWithoutAnnotation {
		public void test() {}
	}

	@UnitTest
	public static class TestWithAnnotation {
		@Test
		public void test() {
			Assert.assertTrue(true);
		}
	}

	@IntegrationTest
	public static class IntegrationTestWithAnnotation {
		@Test
		public void test() {
			Assert.assertTrue(true);
		}
	}

	@UnitTest
	public static class ParentClassWithAnnotation {
		public void nonTestMethod() {}
	}

	public static class ChildTest extends TestAnnotationCheck.ParentClassWithAnnotation {
		@Test
		public void test() {
			Assert.assertTrue(true);
		}
	}
}
