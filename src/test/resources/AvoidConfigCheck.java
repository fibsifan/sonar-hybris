import de.hybris.platform.util.Config;

/**
 * This file is the sample code against we run our unit test.
 * It is placed src/test/resources in order to not be part of the compilation.
 **/
class AvoidConfig {
	public void test() {
		double d = Config.getDouble("key", 0.0); // Noncompliant {{Use the corresponding method of configurationService.getConfig() instead.}}
		char c = Config.getChar("key", 0); // Noncompliant {{Use the corresponding method of configurationService.getConfig() instead.}}
		int i = Config.getInt("key", 0); // Noncompliant {{Use the corresponding method of configurationService.getConfig() instead.}}
		boolean b = Config.getBoolean("key", false); // Noncompliant {{Use the corresponding method of configurationService.getConfig() instead.}}
		String s = Config.getString("key", ""); // Noncompliant {{Use the corresponding method of configurationService.getConfig() instead.}}
		String db = Config.getDatabase();
	}
}
