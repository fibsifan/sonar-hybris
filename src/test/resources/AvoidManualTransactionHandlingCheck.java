import de.hybris.platform.tx.Transaction;
import de.hybris.platform.tx.TransactionBody;

/**
 * This file is the sample code against we run our unit test.
 * It is placed src/test/resources in order to not be part of the compilation.
 **/
class AvoidManualTransactionHandlingCheck {
	public void wrong() {
		final Transaction transaction = Transaction.current();
		transaction.begin(); // Noncompliant {{Avoid manual transaction handling and use Transaction.execute() instead.}}
		try {
			// do some shifty stuff here.
			transaction.commit(); // Noncompliant {{Avoid manual transaction handling and use Transaction.execute() instead.}}
		} catch(Exception e) {
			transaction.rollback(); // Noncompliant {{Avoid manual transaction handling and use Transaction.execute() instead.}}
		}
	}

	public void better() {
		final Transaction transaction = Transaction.current();
		Object result = null;
		try {
			result = transaction.execute(new TransactionBody() {
				@Override
				public <T> T execute() {
					// do some shifty stuff here
					return null;
				}
			});
		} catch (Exception e) {
			// transaction is already rolled back, do the rest of error handling.
		}
	}
}
