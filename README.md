# Hybris Java Rules for Sonar

[![pipeline status](https://gitlab.com/fibsifan/sonar-hybris/badges/master/pipeline.svg)](https://gitlab.com/fibsifan/sonar-hybris/-/commits/master)
[![coverage report](https://gitlab.com/fibsifan/sonar-hybris/badges/master/coverage.svg)](https://gitlab.com/fibsifan/sonar-hybris/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=fibsifan_sonar-hybris&metric=alert_status)](https://sonarcloud.io/dashboard?id=fibsifan_sonar-hybris)

This sonar plugin provides rules for java code in hybris projects.

The plugin is in early alpha status.

## Build

Build with `./mvnw compile` or `mvn compile`.

## Install

Copy the resulting jar file into sonarqube's `plugins` directory.
Depends on sonar's java-plugin.
